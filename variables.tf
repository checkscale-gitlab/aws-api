variable "api_http_id" {
  type        = string
  description = "API Gateway's ID"
}

variable "route" {
  type        = string
  description = "API Route path"
}

variable "method" {
  type        = string
  default     = "POST"
  description = "API Route method"
}

variable "authorizer_id" {
  type        = string
  default     = ""
  description = "API JWT Authorizer's ID"
}

variable "lambda_name" {
  type        = string
  description = "Lambda function name"
}

variable "lambda_dev" {
  type        = string
  default     = "dev"
  description = "Qualifier of Lambda DEV version"
}

variable "lambda_prod" {
  type        = string
  default     = "prod"
  description = "Qualifier of Lambda PROD version"
}
