# AWS Api Gateway

This is a Terraform module to build :
- Lambda function
- API *route* in an existing AWS API Gateway v2 (HTTP) with Lambda integration

Parameters needed :
- **api_http_id** : API Gateway's ID
- **route** : API Route path (eg "/path/to/route")
- **method** : API Route method (default value : POST)
- **authorizer_id** : API JWT Authorizer's ID (optional : if not present, no Authorizer will be configured)
- **lamba_name** : Lambda function name
- **lambda_dev** : Qualifier of Lambda DEV version
- **lambda_prod** : Qualifier of Lambda PROD version

Output values :
- **endpoint_dev** : Api DEV url
- **endpoint_prod** : Api PROD url
